# E1. Exercise 1. Operating Systems Overview

## Introduction

There are mainly two approaches to administer an Operating System:

- Using command line interface
- With the graphical user interface

The quickest one it is usually the CLI, as you could also automate actions through scripting. Linux and bash scripting is the main Operating System and language to reach that goal, but other languages like python are getting popular.

## Contents

Using IsardVDI (or if not available use VirtualBox) create three different virtual machines:

- Centos 7
- Debian 9
- Windows 7

Start each one and reproduce the next administration actions:

- Show disk usage
- Show hardware resource consumption
- Show storage devices in system
- Create a new text file named 'ohmygod.md' and fill it with your personal information.

## Delivery

Answer following questions regarding each Operating System:

1. **Which command (or commands) can be used to show disk usage on this OS using CLI?**

   - Centos 7 CLII (terminal/terminator): 
        - **df**
   - Debian 9 CLI (terminal/terminator):
        - **df**
   - Windows 7 CLI (cmd):  
        - **DISKPART** **List Disk**

2. **Where can you find disk usage in this OS using GUI?**

   - Centos 7 GUI:
        - **By default centOS doesn't have a GUI disk usage program, you can install gnome-disks with dnf install gnome-disks**
   - Debian 9 GUI:
        - **udisk**
   - Windows GUI:
        - **Diskmgmt.msc**

3. **Which command (or commands) show hardware resource usage?**

   - Linux:
        - **top or htop**
   - Windows:
        - **CPU Usage:  C:>wmic cpu get**
        - **loadpercentage**
        - **LoadPercentage 0**
        - **RAM Usage: "systeminfo" and find "Available Physical Memory"**
        - **Network stats:netstat***

4. **How can we show storage devices detected on our system?**

   - Linux:
        - **lsblk**
   - Windows: 
        - **Going to my computer or DISKPART LIST DISK**

5. **Which commands (or applications) can be used to create a new text file?**

   - Linux: 
        - **vi filename.txt**
   - Windows:
        - **echo.>filename.txt**

6. **When will updates will be downloaded and applied on each Operating System by default?**

   - Centos 7:
        - **Whenever you choose to. Also, you can see which packages are available as sudo with "apt-get -u upgrade --assume-no" without installing them**
   - Debian 9:
        - **Whenever you choose to. Also, you can see which packages are available with "dnf check-update" without installing them**
   - Windows:
        - **Automatically when the O.S wants or using wuauclt.exe /detectnow  or going to Windows Update in the Control Panel and scan for updates**
7. **How can we trigger updates manually?**

   - Centos 7:
        - **# dnf upgrade**
   - Debian 9:
        - **# apt-get update && apt-get upgrade**
   - Windows:
        - **“wuauclt.exe /detectnow /updatenow” or going to Windows Update in the Control Panel, scan for updates and Install Now**

8. **Why are Operating System updates so important?**
   - **It usually patches security flaws that could compromise personal information and fixes some errors of the Operating System**
   

9. **Describe the process/command(s) to follow to install GIMP photo editor in each Operating System:**

   - Debian 9:
        - **su -**
        - **add-apt-repository ppa:otto-kesselgulasch/gimp**
        - **apt-get update**
        - **apt-get install gimp**




   

   - Windows:
	    - **https://download.gimp.org/mirror/pub/gimp/v2.10/windows/gimp-2.10.6-setup.exe**
	    - **Select Language --> Accept and click on Install**

10. **What are repositories on a linux distribution?**
    -  **A repository is a collection of software on a server.The software can be downloaded with a package manager,for example Aptitude on Debian or Dandified Packaging Tool in Fedora**


****
